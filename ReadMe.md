### Build instruction ###

1. Update Xcode

2. Install cocoapods with `sudo gem install cocoapods`.

    The current version of foodsharing-ios app depends on the following external libraries:
    
    - MessageKit
    - SocketIO
    
    They are managed by [Cocoapods](https://guides.cocoapods.org/using/getting-started.html#toc_3). You need to install this first.

    Podfile has been created in the root directory. After installation, go to project directory, run `pod install`.

3. After successfully building the libraries, open "foodsharing-ios.xcworkspace".

4. Change team to your appleid, update the bundle name to: [yourappleid]-foodshareing-ios, then build.
