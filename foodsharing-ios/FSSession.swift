//
//  FSSession.swift
//  foodsharing-ios
//
//  Created by Steven on 4/18/20.
//

import Alamofire
import SwiftyUserDefaults

private class FSInterceptor: RequestInterceptor {
  private typealias RefreshCompletion = (_ succeeded: Bool) -> Void

  private let baseURLString: String
  private let lock = NSLock()
  private var isRefreshing = false
  private var requestsToRetry: [(RetryResult) -> Void] = []

  private let session: Session = {
    let configuration = URLSessionConfiguration.af.default
    configuration.httpCookieStorage = PersistentCookieStore.sharedStore
    return Session(configuration: configuration)
  }()

  init(baseURLString: String) {
    self.baseURLString = baseURLString
  }

  func adapt(_ urlRequest: URLRequest,
             for session: Session,
             completion: @escaping (Result<URLRequest, Error>) -> Void) {
    guard let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) else {
      completion(.success(urlRequest))
      return
    }
    var urlRequest = urlRequest
    addToken(to: &urlRequest, from: session.sessionConfiguration.httpCookieStorage)
    completion(.success(urlRequest))
  }

  func retry(_ request: Request,
             for session: Session,
             dueTo error: Error,
             completion: @escaping (RetryResult) -> Void) {
    lock.lock(); defer { lock.unlock() }

    guard (request.request?.url?.absoluteString.lowercased() ?? "").hasPrefix(baseURLString.lowercased()),
      (request.task?.response as? HTTPURLResponse)?.statusCode == 401,
      let credentials = Keychain.codable(key: .credentials, of: Credentials.self) else {
        completion(.doNotRetry)
        return
    }

    requestsToRetry.append(completion)
    if !isRefreshing {
      refreshTokens(credentials: credentials) { [weak self] succeeded in
        guard let self = self else { return }
        self.lock.lock(); defer { self.lock.unlock() }
        self.requestsToRetry.forEach { $0(succeeded ? .retryWithDelay(0.05) : .doNotRetry) }
        self.requestsToRetry.removeAll()
      }
    }
  }

  private func refreshTokens(credentials: Credentials, completion: @escaping RefreshCompletion) {
    guard !isRefreshing else { return }
    PersistentCookieStore.sharedStore.clear()
    isRefreshing = true
    session.request(APIRouter.login(credentials: credentials))
      .responseDecodable(of: User.self) { [weak self] response in
        guard let self = self else { return }
        switch response.result {
          case .success(let user):
            Defaults[\.currentUser] = user
            completion(true)
          case .failure:
            completion(false)
        }
        self.isRefreshing = false
      }
  }

  private func addToken(to request: inout URLRequest, from cookieStorage: HTTPCookieStorage?) {
    guard let cookieStorage = cookieStorage else { return }
    guard let cookies = cookieStorage.cookies(for: Constants.baseUrl) else { return }
    guard let tokenCookie = cookies.first(where: { $0.name == Constants.tokenCookieName }) else { return }
    request.addValue(tokenCookie.value, forHTTPHeaderField: Constants.tokenHeaderName)
  }
}

let FSSession: Session = {
  let configuration = URLSessionConfiguration.af.default
  configuration.httpCookieStorage = PersistentCookieStore.sharedStore
  let fsInterceptor = FSInterceptor(baseURLString: Constants.baseUrl.absoluteString)
  return Session(configuration: configuration, interceptor: Interceptor(interceptors: [fsInterceptor]))
}()
