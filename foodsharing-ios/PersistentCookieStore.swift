//
//  PersistentCookieStore.swift
//  foodsharing-ios
//
//  Created by Steven on 4/19/20.
//

import Foundation

class PersistentCookieStore: HTTPCookieStorage {

  static let sharedStore = PersistentCookieStore()

  override func storeCookies(_ cookies: [HTTPCookie], for task: URLSessionTask) {
    super.storeCookies(cookies, for: task)
    saveIfNeeded(cookies: cookies)
  }

  override func setCookies(_ cookies: [HTTPCookie], for URL: URL?, mainDocumentURL: URL?) {
    super.setCookies(cookies, for: URL, mainDocumentURL: mainDocumentURL)
    saveIfNeeded(cookies: cookies)
  }

  // swiftlint:disable:next discouraged_optional_collection
  override func getCookiesFor(_ task: URLSessionTask, completionHandler: @escaping ([HTTPCookie]?) -> Void) {
    super.getCookiesFor(task) { cookies in
      completionHandler(self.addTokens(to: cookies ?? []))
    }
  }

  // swiftlint:disable:next discouraged_optional_collection
  override func cookies(for URL: URL) -> [HTTPCookie]? {
    addTokens(to: super.cookies(for: URL) ?? [])
  }

  func clear() {
    Keychain.remove(key: .tokenCookie)
    Keychain.remove(key: .sessionIdCookie)
    cookies?.forEach {
      deleteCookie($0)
    }
  }

  private func saveIfNeeded(cookies: [HTTPCookie]) {
    guard let tokenCookie = cookies.first(where: { $0.name == Constants.tokenCookieName }) else { return }
    guard let sessionCookie = cookies.first(where: { $0.name == Constants.sessionCookieName }) else { return }
    Keychain.set(key: .sessionIdCookie, nsCoding: sessionCookie)
    Keychain.set(key: .tokenCookie, nsCoding: tokenCookie)
  }

  private func addTokens(to cookies: [HTTPCookie]) -> [HTTPCookie] {
    var cookies = cookies
    if !cookies.contains(where: { $0.name == Constants.tokenCookieName })
      && !cookies.contains(where: { $0.name == Constants.sessionCookieName }) {
      if let tokenCookie = Keychain.nsCoding(key: .tokenCookie, of: HTTPCookie.self),
        let sessionIdCookie = Keychain.nsCoding(key: .sessionIdCookie, of: HTTPCookie.self) {
        cookies.append(tokenCookie)
        cookies.append(sessionIdCookie)
      }
    }
    return cookies
  }
}
