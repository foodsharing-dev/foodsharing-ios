//
//  socket-io.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 23.04.19.
//

import Foundation
import SocketIO

class SocketIOManager {

  static let sharedInstance = SocketIOManager()

  let manager = SocketManager(socketURL: Constants.baseUrl)

  lazy var socket = manager.defaultSocket

  init() {

    let token = Keychain.nsCoding(key: .tokenCookie, of: HTTPCookie.self)?.value
    let sessionId = Keychain.nsCoding(key: .sessionIdCookie, of: HTTPCookie.self)?.value
    // If token or sessionId are not available to this time. The verification failed miserably
    let cookie = "CSRF_TOKEN=" + token! + "; PHPSESSID=" + sessionId!

    manager.config = SocketIOClientConfiguration(arrayLiteral: .path("/chat/socket.io/"),
                                                 .log(true),
                                                 .compress,
                                                 .forceWebsockets(true),
                                                 .secure(true),
                                                 .extraHeaders(["X-CSRF-Token": token!, "cookie": cookie])
    )

    socket.on("connect") { _, _ in
      self.socket.emit("register")
    }

    socket.on("conv") { data, _ in
      NotificationCenter.default.post(name: .gotConvFromSocket, object: data)
    }
  }

  func establishConnection() {
    socket.connect()
  }

  func closeConnection() {
    socket.disconnect()
  }

  func onclickbutton() {
    socket.emit("message", "GEKK")
  }
}
