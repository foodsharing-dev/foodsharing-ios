//
//  APIRouter.swift
//  foodsharing-ios
//
//  Created by Steven on 4/18/20.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
  case login(credentials: Credentials)
  case listConversations(number: Int, offset: Int)
  case getConversation(id: Int, number: Int)
  case sendMessage(conversationId: Int, message: String)
  case getCurrentUser
  case logout

  var path: URL {
    let base = Constants.baseUrl
    switch self {
      case .login: return base.appendingPathComponent("/api/user/login")
      case .getCurrentUser: return base.appendingPathComponent("/api/user/current")
      case .logout: return base.appendingPathComponent("/api/user/logout")
      case .listConversations: return base.appendingPathComponent("/api/conversations")
      case .getConversation(let id, _): return base.appendingPathComponent("/api/conversations/\(id)")
      // workaround to prevent enconding "?"
      case .sendMessage(let id, _): return base.appendingPathComponent("/api/conversations/\(id)/messages")
    }
  }

  var headers: HTTPHeaders? {
    switch self {
      default: return nil
    }
  }

  var method: HTTPMethod {
    switch self {
      case .login, .logout, .sendMessage: return .post
      case .getCurrentUser, .listConversations, .getConversation: return .get
    }
  }

  var parameters: Parameters? {
    switch self {
      case .login(let credentials): return ["email": credentials.email, "password": credentials.password]
      case .listConversations(let limit, let offset): return ["limit": limit, "offset": offset]
      case .getConversation(_, let limit): return ["messagesLimit": limit]
      case .sendMessage(_, let message): return ["body": message]
      default: return nil
    }
  }

  var encoding: ParameterEncoding {
    switch self {
      case .login, .sendMessage: return URLEncoding(destination: .httpBody)
      case .listConversations, .getConversation: return URLEncoding(destination: .queryString)
      default: fatalError("Not Impelemnted.")
    }
  }

  func asURLRequest() throws -> URLRequest {
    var request = try URLRequest(url: path, method: method, headers: headers)
    request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    if let parameters = parameters { request = try encoding.encode(request, with: parameters) }
    return request
  }
}
