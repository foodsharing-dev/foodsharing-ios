//
//  Prefs.swift
//  foodsharing-ios
//
//  Created by Steven on 4/18/20.
//

import SwiftyUserDefaults

extension DefaultsKeys {
  var firstAppRun: DefaultsKey<Bool> { .init("firstAppRun", defaultValue: true) }
  var currentUser: DefaultsKey<User?> { .init("currentUser") }
}
