//
//  LibsManager.swift
//  foodsharing-ios
//
//  Created by Mahmoud on 5/28/20.
//

import IQKeyboardManagerSwift

enum LibsManager {

  static func setupLibs() {
    setupTheme()
    setupKeyboardManager()
    setupRSwift()
  }

  private static func setupTheme() {
    UINavigationBar.appearance().tintColor = .white
    UINavigationBar.appearance().prefersLargeTitles = false
    UINavigationBar.appearance().isTranslucent = false
    UINavigationBar.appearance().barTintColor = Colors.fsBrown()
    UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
    UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]
  }

  private static func setupKeyboardManager() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
    IQKeyboardManager.shared.resignFirstResponder()
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    IQKeyboardManager.shared.enableAutoToolbar = false
    IQKeyboardManager.shared.disabledDistanceHandlingClasses = [ConversationVC.self]
  }

  private static func setupRSwift() {
    do {
      try R.validate()
    } catch {
      fatalError("R.swift error: \(error.localizedDescription)")
    }
  }
}
