//
//  RootTabBarController.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import Swinject
import UIKit

final class MainTabBarController: BaseTabBarController {

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemBackgroundCompat
    delegate = self
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    setupTabs()
  }

  private func setupTabs() {
    guard (viewControllers ?? []).isEmpty else { return }
    let messagesListVC = Assembler.mainAssembler.resolver.resolve(ConversationsListVC.self)!
    messagesListVC.tabBarItem = UITabBarItem(title: nil, image: Images.chat()!, selectedImage: nil)
    messagesListVC.title = "Chat"
    let messagesNavController = BaseNC(rootViewController: messagesListVC)

    let basketsVC = Assembler.mainAssembler.resolver.resolve(BasketsVC.self)!
    basketsVC.tabBarItem = UITabBarItem(title: nil, image: Images.basket()!, selectedImage: nil)
    basketsVC.title = "My Baskets"
    let basketsNavController = BaseNC(rootViewController: basketsVC)

    let mapVC = Assembler.mainAssembler.resolver.resolve(MapVC.self)!
    mapVC.tabBarItem = UITabBarItem(title: nil, image: Images.location()!, selectedImage: nil)
    mapVC.title = "Map"

    let moreVC = Assembler.mainAssembler.resolver.resolve(MoreVC.self)!
    moreVC.tabBarItem = UITabBarItem(title: nil, image: Images.more()!, selectedImage: nil)
    moreVC.title = "More"
    let moreNavController = BaseNC(rootViewController: moreVC)

    setViewControllers([
      messagesNavController,
      basketsNavController,
      mapVC,
      moreNavController,
    ], animated: true)
  }
}

extension MainTabBarController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController,
                        shouldSelect viewController: UIViewController) -> Bool {

    guard let selectedViewController = selectedViewController,
      selectedViewController != viewController,
      let fromView = selectedViewController.view,
      let toView = viewController.view else { return true }

    UIView.transition(from: fromView,
                      to: toView,
                      duration: 0.25,
                      options: .transitionCrossDissolve,
                      completion: nil)
    return true
  }
}
