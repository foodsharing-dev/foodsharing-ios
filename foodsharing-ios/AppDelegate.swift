//
//  AppDelegate.swift
//  LoginScreenSwift

import SwiftyUserDefaults
import Swinject
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   // swiftlint:disable:next discouraged_optional_collection
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    clearKeyChainOnFirstRun()
    LibsManager.setupLibs()
    instantiateWindow()
    return true
  }

  @available(iOS 13.0, *)
  func application(_ application: UIApplication,
                   configurationForConnecting connectingSceneSession: UISceneSession,
                   options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    return UISceneConfiguration(name: connectingSceneSession.configuration.name!,
                                sessionRole: connectingSceneSession.role)
  }

  private func instantiateWindow() {
    if #available(iOS 13.0, *) {
      print("SceneDelegate should instantiate the window instead of AppDelegate")
      return
    }
    print("instantiating legacy window")
    window = FSWindow(frame: UIScreen.main.bounds)
    window!.rootViewController = Assembler.mainAssembler.resolver.resolve(SplashVC.self)!
    window!.makeKeyAndVisible()
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    SocketIOManager.sharedInstance.closeConnection()
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    SocketIOManager.sharedInstance.establishConnection()
  }

  private func clearKeyChainOnFirstRun() {
    if Defaults[\.firstAppRun] {
      Keychain.clear()
    }
    Defaults[\.firstAppRun] = false
  }
}
