//
//  SplashVC.swift
//  
//
//  Created by Steven on 5/31/20.
//

import RxSwift
import Swinject
import UIKit

class SplashVC: BaseVC {

  @IBOutlet private weak var activityView: UIActivityIndicatorView!

  let vm: SplashVM
  let bag = DisposeBag()

  override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }

  init(vm: SplashVM) {
    self.vm = vm
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    if #available(iOS 13.0, *) {
      activityView.style = .large
    } else {
      activityView.style = .whiteLarge
    }
    activityView.color = .systemGray3Compat
    setupBindings()
    vm.reload()
  }

  func setupBindings() {
    vm.isLoggedIn.drive(onNext: startApp).disposed(by: bag)
  }

  func startApp(isLoggedIn: Bool) {
    if !isLoggedIn {
      Keychain.clear()
      PersistentCookieStore.sharedStore.clear()
    }
    let vc: UIViewController
    if isLoggedIn {
      vc = Assembler.mainAssembler.resolver.resolve(MainTabBarController.self)!
    } else {
      vc = Assembler.mainAssembler.resolver.resolve(LoginVC.self)!
    }
    (view.window as! FSWindow).setAnimatedRootVC(viewController: vc)
  }
}
