//
//  SplashVM.swift
//  foodsharing-ios
//
//  Created by Steven on 6/8/20.
//

import RxCocoa
import RxSwift

class SplashVM {

  private let api: API
  private let reloadTrigger = PublishSubject<Void>()
  let isLoggedIn: Driver<Bool>

  init(api: API) {
    self.api = api
    let credentials = Keychain.codable(key: .credentials, of: Credentials.self)
    let obs: Observable<Bool>
    if credentials == nil {
      obs = reloadTrigger.flatMapLatest {
        Observable.just(false)
      }
    } else {
      obs = reloadTrigger.flatMapLatest {
        api.currentUser()
      }.map { _ in
        true
      }
    }
    self.isLoggedIn = obs
      .delay(.seconds(1), scheduler: RxSchedulers.serialUserInitiated)
      .asDriver(onErrorJustReturn: false)
  }

  func reload() {
    reloadTrigger.onNext(())
  }
}
