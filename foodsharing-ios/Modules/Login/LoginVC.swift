//
//  LoginVC.swift
//  LoginScreenSwift
//

import SafariServices
import SwiftyUserDefaults
import Swinject
import UIKit

final class LoginVC: BaseVC {

  override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }

  @IBOutlet private weak var errorLbl: UILabel!
  @IBOutlet private weak var loginBtn: UIButton!
  @IBOutlet private weak var emailTF: UITextField!
  @IBOutlet private weak var passwordTF: PasswordTextField!
  @IBOutlet private weak var forgotPasswordBtn: UIButton!
  @IBOutlet private weak var registerBtn: UIButton!

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setNeedsStatusBarAppearanceUpdate()
    setupUI()
  }

  private func setupUI() {
    emailTF.placeholder = Strings.email()
    passwordTF.placeholder = Strings.password()
    forgotPasswordBtn.setTitle(Strings.forgotPassword(), for: .normal)
    registerBtn.setTitle(Strings.register(), for: .normal)
    loginBtn.setTitle(Strings.login(), for: .normal)
    loginBtn.layer.cornerRadius = 4
    loginBtn.clipsToBounds = true
  }

  private func openSafari(with url: URL) {
    let safariVC = SFSafariViewController(url: url)
    present(safariVC, animated: true, completion: nil)
  }

  @IBAction
  private func loginButtonClicked(_ sender: Any) {
    guard let email = emailTF.text?.trimmed, !email.isEmpty,
      let password = passwordTF.text?.trimmed, !password.isEmpty else { return }
    login(email: email, password: password)
  }

  @IBAction
  private func didPressRegisterBtn(_ sender: UIButton) {
    openSafari(with: Constants.registerationUrl)
  }

  @IBAction
  private func didPressForgotPasswordBtn(_ sender: UIButton) {
    openSafari(with: Constants.forgotPasswordUrl)
  }
}

extension LoginVC {
  func login(email: String, password: String) {
    errorLbl.text = ""
    let credentials = Credentials(email: email, password: password)
    OldAPI.login(credentials: credentials) { [weak self] response in
      guard let self = self else { return }
      switch response.result {
        case .success(let user):
          Keychain.set(key: .credentials, codable: credentials)
          Defaults[\.currentUser] = user
          self.startApp()
        case .failure:
          self.errorLbl.text = response.response?.statusCode == 401 ? Strings.invalidPassword() : Strings.unknownError()
      }
    }
  }

  func startApp() {
    UIApplication.shared.windows.compactMap { $0 as? FSWindow }.forEach {
      $0.setAnimatedRootVC(viewController: Assembler.mainAssembler.resolver.resolve(SplashVC.self)!)
    }
  }
}
