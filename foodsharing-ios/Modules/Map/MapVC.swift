//
//  MapVC.swift
//  foodsharing-ios
//
//  Created by Mahmoud on 4/30/20.
//

import UIKit

final class MapVC: BaseVC {

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }
}
