//
//  MoreVC.swift
//  foodsharing-ios
//
//  Created by Mahmoud on 4/30/20.
//

import Swinject
import UIKit

final class MoreVC: BaseVC {

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  @IBAction
  private func logoutAction(_ sender: UIButton) {
    FSSession.request(APIRouter.logout).response { response in
      print("logout response: \(response.response?.statusCode ?? -1)")
      Keychain.clear()
      PersistentCookieStore.sharedStore.clear()
      UIApplication.shared.windows.compactMap { $0 as? FSWindow }.forEach {
        $0.setAnimatedRootVC(viewController: Assembler.mainAssembler.resolver.resolve(SplashVC.self)!)
      }
    }
  }
}
