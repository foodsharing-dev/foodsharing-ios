//
//  ConversationVC.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 08.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import InputBarAccessoryView
import MessageKit
import Nuke
import SwiftyUserDefaults
import UIKit

final class ConversationVC: MessagesViewController {

  var conversationId: Int!
  var conversationName: String?
  private var conversation: Conversation?
  private var messages: [Message] { conversation?.messages ?? [] }

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = conversationName

    NotificationCenter.default.addObserver(
      self,
      selector: #selector(self.handleConvNotification(_:)),
      name: .gotConvFromSocket,
      object: nil
    )

    loadMessages(conversationId: self.conversationId)

    SocketIOManager.sharedInstance.establishConnection()

    configureMessageCollectionView()
    configureMessageInputBar()
  }

  func configureMessageInputBar() {
    messageInputBar.delegate = self
    messageInputBar.inputTextView.tintColor = Colors.fsBrown()!
    messageInputBar.sendButton.setTitleColor(Colors.fsBrown()!, for: .normal)
    messageInputBar.sendButton.setTitle(Strings.send(), for: .normal)
    messageInputBar.inputTextView.placeholder = Strings.message()
    messageInputBar.sendButton.setTitleColor(
      Colors.fsBrown()!.withAlphaComponent(0.5),
      for: .highlighted
    )
  }

  func configureMessageCollectionView() {
    messagesCollectionView.messagesDataSource = self
    messagesCollectionView.messageCellDelegate = self
    messagesCollectionView.messagesDisplayDelegate = self
    messagesCollectionView.messagesLayoutDelegate = self

    scrollsToBottomOnKeyboardBeginsEditing = false
    maintainPositionOnKeyboardFrameChanged = true
  }
}

extension ConversationVC {
  func loadMessages(conversationId: Int) {
    OldAPI.conversationDetail(id: conversationId, number: 50) {
      switch $0 {
        case .success(let data):
          self.conversation = data.toConversation()
          self.messagesCollectionView.reloadData()
          self.messagesCollectionView.scrollToBottom(animated: false)
        default:
          break
      }
    }
  }
}

extension ConversationVC: MessagesDataSource {
  func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
    return messages.count
  }

  func currentSender() -> SenderType {
    return Defaults[\.currentUser]!
  }

  func messageForItem(at indexPath: IndexPath,
                      in messagesCollectionView: MessagesCollectionView) -> MessageType {

    return messages[indexPath.section]
  }

  func messageTopLabelAttributedText(for message: MessageType,
                                     at indexPath: IndexPath) -> NSAttributedString? {

    return NSAttributedString(
      string: message.sender.displayName,
      attributes: [.font: UIFont.systemFont(ofSize: 13)]
    )
  }

  func messageBottomLabelAttributedText(for message: MessageType,
                                        at indexPath: IndexPath) -> NSAttributedString? {

    return NSAttributedString(
      string: MessageKitDateFormatter.shared.string(from: message.sentDate),
      attributes: [.font: UIFont.systemFont(ofSize: 10), .foregroundColor: UIColor.gray]
    )
  }
}

extension ConversationVC: MessagesLayoutDelegate {
  func messageTopLabelHeight(for message: MessageType,
                             at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    return 20
  }

  func messageBottomLabelHeight(for message: MessageType,
                                at indexPath: IndexPath,
                                in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    return 16
  }
}

extension ConversationVC: MessagesDisplayDelegate {
  func configureAvatarView(_ avatarView: AvatarView,
                           for message: MessageType,
                           at indexPath: IndexPath,
                           in messagesCollectionView: MessagesCollectionView) {
    avatarView.backgroundColor = .systemGray4Compat
    let imgUrl = getUserPhotoURL(for: messages[indexPath.section].author)
    Nuke.loadImage(with: imgUrl, into: avatarView)
  }

  func messageStyle(for message: MessageType,
                    at indexPath: IndexPath,
                    in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .pointedEdge)
  }

  func backgroundColor(for message: MessageType,
                       at indexPath: IndexPath,
                       in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? Colors.fsBrown()! : .systemGray5Compat
  }

  // implement this to enable url,phone,address detection in a chat cell
  func enabledDetectors(for message: MessageType,
                        at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
    return [.url, .phoneNumber]
  }

  // use this to style a detected attribute like a hashtag or url in a diffrent color
  func detectorAttributes(for detector: DetectorType,
                          and message: MessageType,
                          at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
    return [.foregroundColor: UIColor.blue]
  }
}

extension ConversationVC: MessageCellDelegate {

  func didSelectURL(_ url: URL) {
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }

  func didSelectPhoneNumber(_ phoneNumber: String) {
    if let url = URL(string: "tel://\(phoneNumber)") {
      if UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url)
      }
    }
  }
}

extension ConversationVC: InputBarAccessoryViewDelegate {
  func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
    let originalText = text
    let originalPlaceholder = messageInputBar.inputTextView.placeholder
    messageInputBar.inputTextView.text = ""
    messageInputBar.sendButton.startAnimating()
    messageInputBar.inputTextView.placeholder = Strings.sending() + ".."
    FSSession.request(APIRouter.sendMessage(conversationId: conversationId, message: text))
      .response { [weak self] response in
        guard let self = self else { return }
        self.messageInputBar.inputTextView.text = response.response?.statusCode == 200 ? "" : originalText
        self.messageInputBar.inputTextView.placeholder = originalPlaceholder
        self.messageInputBar.sendButton.stopAnimating()
        self.messagesCollectionView.scrollToBottom(animated: true)
      }
  }
}

extension ConversationVC {
  @objc
  func handleConvNotification(_ notification: Notification) {
    guard let test = (notification.object as? [[String: Any]])?.first else { return }
    guard let type = test["m"] as? String, type.trimmed.lowercased() == "push" else { return }
    guard let object = test["o"],
      let jsonData = try? JSONSerialization.data(withJSONObject: object, options: []) else { return }
    let responseDecoded: SocketResponse
    do {
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .fsJsonDateDecoder
      responseDecoded = try decoder.decode(SocketResponse.self, from: jsonData)
    } catch {
      print(error.localizedDescription)
      return
    }
    // check if its this conversation
    guard responseDecoded.cid == conversation?.id else { return }
    let message = responseDecoded.toMessage(from: conversation!.members)
    conversation!.messages.append(message)
    messagesCollectionView.reloadData()
    messagesCollectionView.scrollToBottom(animated: false)
  }
}
