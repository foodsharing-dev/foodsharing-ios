//
//  MessageListVC.swift
//  foodsharing-ios
//
//  Created by Steven on 4/24/20.
//

import RxCocoa
import RxSwift
import SwiftyUserDefaults
import Swinject
import UIKit

final class ConversationsListVC: BaseVC {

  @IBOutlet private weak var tableView: UITableView!

  private var conversations: [ConversationsListItemVM] = []

  private let vm: ConversationsListVM
  private let bag = DisposeBag()

  init(vm: ConversationsListVM) {
    print("init ConversationsListVC")
    self.vm = vm
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Chat"
    setupUI()
    setupBindings()
    vm.reloadTrigger.onNext(())
  }

  private func setupUI() {
    tableView.tableHeaderView = UIView()
    tableView.tableFooterView = UIView()
    tableView.register(Nibs.conversationsListItemCell)
    tableView.rowHeight = 80
    tableView.delegate = self
    tableView.dataSource = self
    tableView.refreshControl = UIRefreshControl()
  }

  private func setupBindings() {
    tableView.refreshControl!.rx.controlEvent(.valueChanged)
      .bind(to: vm.reloadTrigger)
      .disposed(by: bag)
    vm.isLoadingVisible
      .drive(tableView.refreshControl!.rx.isRefreshing)
      .disposed(by: bag)
    vm.conversations
      .observeOn(RxSchedulers.serialMain)
      .subscribe(onNext: {
        self.conversations = $0
        self.tableView.reloadData()
      }).disposed(by: bag)
  }
}

extension ConversationsListVC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.conversationsListItemCell,
                                             for: indexPath)!
    cell.setup(with: conversations[indexPath.row])
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return conversations.count
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    defer { tableView.deselectRow(at: indexPath, animated: true) }
    let vc = Assembler.mainAssembler.resolver.resolve(ConversationVC.self)!
    vc.conversationId = conversations[indexPath.row].id
    navigationController!.pushViewController(vc, animated: true)
  }
}
