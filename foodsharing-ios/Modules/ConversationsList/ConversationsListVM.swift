//
//  MessageListVM.swift
//  foodsharing-ios
//
//  Created by Steven on 4/26/20.
//

import RxCocoa
import RxSwift
import SwiftyUserDefaults

class ConversationsListVM {

  let reloadTrigger = PublishSubject<Void>()
  var conversations: Observable<[ConversationsListItemVM]>!
  private let _isLoadingVisible = BehaviorSubject<Bool>(value: false)
  var isLoadingVisible: Driver<Bool> { _isLoadingVisible.asDriver(onErrorJustReturn: false) }
  // TODO: should be an observable
  var currentUser: User? = Defaults[\.currentUser]

  private let api: API

  init(api: API) {
    print("init ConversationsListVM")
    self.api = api
    conversations = reloadTrigger.flatMapLatest {
      api.listConversations(number: 20, offset: 0)
    }.observe(on: RxSchedulers.serialUserInitiated).map {
      $0.toConversations()
    }.map {
      $0.map { ConversationsListItemVM(conversation: $0, currentUserId: self.currentUser!.id) }
    }.do(onNext: { _ in
      self._isLoadingVisible.onNext(false)
    })
  }

  func forceRefresh() {
    _isLoadingVisible.onNext(true) // should make refresh indicator appear but it doesn't
    reloadTrigger.onNext(())
  }
}
