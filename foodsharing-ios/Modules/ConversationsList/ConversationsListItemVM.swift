//
//  MessageListItemVM.swift
//  foodsharing-ios
//
//  Created by Steven on 4/26/20.
//

import Foundation

struct ConversationsListItemVM {

  let id: Int
  let name: String
  let date: String
  let personImage: URL
  let message: String
  let hasUnreadMessages: Bool

  init(conversation model: Conversation, currentUserId: Int) {
    self.id = model.id
    self.name = ConversationsListItemVM.generateName(model, currentUserId)
    self.personImage = getUserPhotoURL(for: model.members.first(where: { $0.id != currentUserId }))
    self.date = ConversationsListItemVM.generateDate(model.lastMessage?.sentAt)
    self.message = ConversationsListItemVM.generateMessage(model, currentUserId)
    self.hasUnreadMessages = model.hasUnreadMessages
  }

  private static func generateName(_ conversation: Conversation, _ currentUserId: Int) -> String {
    let title = conversation.title?.trimmed ?? ""
    if title.isNotEmpty { return title }
    return conversation.members
      .filter { $0.id != currentUserId }
      .compactMap { $0.name?.trimmed }
      .filter { $0.isNotEmpty }
      .joined(separator: ", ")
  }

  private static func generateMessage(_ conversation: Conversation, _ currentUserId: Int) -> String {
    guard let lastMessage = conversation.lastMessage else { return "" }
    let message = lastMessage.body
    let talkerName: String
    if lastMessage.author.id == currentUserId {
      talkerName = Strings.you()
    } else if let lastMemberName = lastMessage.author.name, lastMemberName.isNotEmpty {
      talkerName = lastMemberName
    } else {
      return message
    }
    return "\(talkerName): \(message)"
  }

  private static func generateDate(_ date: Date?) -> String {
    guard let date = date else { return "" }
    return MessageKitDateFormatter.shared.string(from: date, short: true)

  }
}
