//
//  MessageListItemCell.swift
//  LoginScreenSwift
//
//  Created by Baudisgroup User on 07.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Nuke
import RxCocoa
import SwiftyUserDefaults
import UIKit

class ConversationsListItemCell: UITableViewCell {

  @IBOutlet private weak var senderLabel: UILabel!
  @IBOutlet private weak var lineLabel: UILabel!
  @IBOutlet private weak var timeLabel: UILabel!
  @IBOutlet private weak var chatImageView: UIImageView!

  func setup(with vm: ConversationsListItemVM) {
    Nuke.loadImage(with: vm.personImage, into: chatImageView)
    senderLabel.text = vm.name
    timeLabel.text = vm.date
    lineLabel.text = vm.message
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    contentView.layoutIfNeeded()
    chatImageView.layer.cornerRadius = chatImageView.frame.width / 2
  }
}
