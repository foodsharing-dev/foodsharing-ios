//
//  BasketsVC.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 13.06.19.
//

import UIKit

final class BasketsVC: BaseVC {

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }
}
