//
//  BaseNC.swift
//  foodsharing-ios
//
//  Created by Steven on 5/31/20.
//

import UIKit

class BaseNC: UINavigationController {
  override var childForStatusBarStyle: UIViewController? { viewControllers.last }
  override var childForStatusBarHidden: UIViewController? { viewControllers.last }
  override var childForHomeIndicatorAutoHidden: UIViewController? { viewControllers.last }
}
