//
//  BaseTabBarController.swift
//  foodsharing-ios
//
//  Created by Steven on 5/31/20.
//

import UIKit

class BaseTabBarController: UITabBarController {
  override var childForStatusBarStyle: UIViewController? { selectedViewController }
  override var childForStatusBarHidden: UIViewController? { selectedViewController }
  override var childForHomeIndicatorAutoHidden: UIViewController? { selectedViewController }
}
