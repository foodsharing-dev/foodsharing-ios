//
//  RelaxedInt.swift
//  foodsharing-ios
//
//  Created by Steven on 4/25/20.
//

struct RelaxedInt: Codable {
  let wrappedValue: Int

  init(_ wrappedValue: Int) {
    self.wrappedValue = wrappedValue
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()
    if let intValue = try? container.decode(Int.self) {
      wrappedValue = intValue
    } else if let stringValue = try? container.decode(String.self).trimmed, let intValue = Int(stringValue) {
      wrappedValue = intValue
    } else {
      throw DecodingError.typeMismatch(Int.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
    }
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    try container.encode(wrappedValue)
  }
}
