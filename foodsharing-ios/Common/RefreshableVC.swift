//
//  RefreshableVC.swift
//  foodsharing-ios
//
//  Created by Steven on 4/24/20.
//

import UIKit

@objc protocol RefreshableVC where Self: UIViewController {
  var refreshableView: UIScrollView { get }
  func refresh()
}

extension RefreshableVC {
  func setupRefreshControl() {
    let refreshControl = UIRefreshControl()
    refreshableView.refreshControl = refreshControl
    refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
  }
}
