//
//  PasswordTextField.swift
//  foodsharing-ios
//
//  Created by Mahmoud on 5/19/20.
//

import UIKit

@IBDesignable
class PasswordTextField: UITextField {

  private var _showedImage = UIImage()
  private var _hiddenImage = UIImage()
  private var revealBtn: UIButton!

  private lazy var revealBtnView: UIView = {
    let revealBtn = UIButton(type: .system)
    revealBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    revealBtn.addTarget(self, action: #selector(didTapRevealBtn(_:)), for: .touchUpInside)
    self.revealBtn = revealBtn
    let view = UIView()
    view.frame = CGRect(x: 0, y: 0, width: 25, height: 20)
    view.addSubview(revealBtn)
    return view
  }()

  override var isSecureTextEntry: Bool {
    didSet { refreshState() }
  }

  override var tintColor: UIColor! {
    didSet { refreshState() }
  }

  @IBInspectable var showedImage: UIImage {
    get { _showedImage }
    set { _showedImage = newValue; refreshState() }
  }

  @IBInspectable var hiddenImage: UIImage {
    get { _hiddenImage }
    set { _hiddenImage = newValue; refreshState() }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    commonInit()
  }

  private func commonInit() {
    rightViewMode = .always
    rightView = revealBtnView
    refreshState()
  }

  @objc
  private func didTapRevealBtn(_ sender: UIButton) {
    isSecureTextEntry.toggle()
  }

  private func refreshState() {
    let image = isSecureTextEntry ? showedImage : hiddenImage
    revealBtn?.tintColor = tintColor
    revealBtn?.setImage(image, for: .normal)
  }
}
