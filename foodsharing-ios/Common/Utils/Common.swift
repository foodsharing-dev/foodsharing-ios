//
//  Common.swift
//  foodsharing-ios
//
//  Created by Steven on 4/23/20.
//

import Foundation
import RxSwift
import UIKit

@discardableResult
func with<T>(_ item: T, block: (inout T) throws -> Void) rethrows -> T {
  var this = item
  try block(&this)
  return this
}

func getUserPhotoURL(for user: User?) -> URL {
  return getUserPhotoURL(for: user?.avatar)
}

func getUserPhotoURL(for image: String?) -> URL {
  guard let userPhotoFilePath = image?.trimmed, userPhotoFilePath.isNotEmpty else {
    return Constants.defaultUserPicutre
  }
  // TODO:- Research: "130_q_" means 130x130, but android and web contains more sizes
  let prefix = userPhotoFilePath.hasPrefix("/api") ? "" : "/images/130_q_"
  return Constants.baseUrl.appendingPathComponent("\(prefix)\(userPhotoFilePath)")
}

enum RxSchedulers {
  static var serialMain: SerialDispatchQueueScheduler { MainScheduler.instance }
  static let serialUserInitiated = SerialDispatchQueueScheduler(qos: .userInitiated)
}

func currentQueueName() -> String? {
  let name = __dispatch_queue_get_label(nil)
  return String(cString: name, encoding: .utf8)
}
