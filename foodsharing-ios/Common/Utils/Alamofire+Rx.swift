//
//  Alamofire+Rx.swift
//  foodsharing-ios
//
//  Created by Steven on 4/26/20.
//

import Alamofire
import RxSwift

extension DataRequest: ReactiveCompatible {}

extension Reactive where Base: DataRequest {

  @discardableResult
  public func responseDecodable<T: Decodable>(of type: T.Type = T.self,
                                              queue: DispatchQueue = .main,
                                              decoder: Alamofire.DataDecoder = JSONDecoder()) -> Single<T> {
    Single<T>.create { observer in
      self.base.responseDecodable(of: type, queue: queue, decoder: decoder) { response in
        switch response.result {
          case .success(let data):
            observer(.success(data))
          case .failure(let error):
          observer(.failure(error))
        }
      }
      return Disposables.create {
        self.base.cancel()
      }
    }
  }
}
