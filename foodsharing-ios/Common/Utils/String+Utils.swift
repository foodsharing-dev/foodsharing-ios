//
//  String+Utils.swift
//  foodsharing-ios
//
//  Created by Steven on 4/23/20.
//

import Foundation

extension String {
  var trimmed: String { trimmingCharacters(in: .whitespacesAndNewlines) }
  var isBlank: Bool { trimmed.isEmpty }
}

extension Optional where Wrapped == String {
  var isNilOrEmpty: Bool { self?.isEmpty ?? true }
  var isNilOrBlank: Bool { self?.isBlank ?? true }
}
