//
//  Collection+Utils.swift
//  foodsharing-ios
//
//  Created by Steven on 4/23/20.
//

import Foundation

extension Collection {
  var isNotEmpty: Bool { !isEmpty }
}

extension Sequence {
  func map<T>(_ keyPath: KeyPath<Element, T>) -> [T] {
    return map { $0[keyPath: keyPath] }
  }

  func sorted<T: Comparable>(on keyPath: KeyPath<Element, T>,
                             by areInIncreasingOrder: (T, T) -> Bool) -> [Element] {
    return sorted { areInIncreasingOrder($0[keyPath: keyPath], $1[keyPath: keyPath]) }
  }
}
