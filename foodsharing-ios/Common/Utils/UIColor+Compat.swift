//
//  UIColor+Compat.swift
//  foodsharing-ios
//
//  Created by Steven on 5/17/20.
//

import UIKit

// Copied and modified from
// https://github.com/noahsark769/ColorCompatibility/blob/master/Sources/ColorCompatibility.swift

extension UIColor {

  public static var labelCompat: UIColor {
    if #available(iOS 13, *) { return .label }
    return UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
  }

  public static var secondaryLabelCompat: UIColor {
    if #available(iOS 13, *) { return .secondaryLabel }
    return UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.6)
  }

  public static var tertiaryLabelCompat: UIColor {
    if #available(iOS 13, *) { return .tertiaryLabel }
    return UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.3)
  }

  public static var quaternaryLabelCompat: UIColor {
    if #available(iOS 13, *) { return .quaternaryLabel }
    return UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.18)
  }

  public static var systemFillCompat: UIColor {
    if #available(iOS 13, *) { return .systemFill }
    return UIColor(red: 0.47058823529411764, green: 0.47058823529411764, blue: 0.5019607843137255, alpha: 0.2)
  }

  public static var secondarySystemFillCompat: UIColor {
    if #available(iOS 13, *) { return .secondarySystemFill }
    return UIColor(red: 0.47058823529411764, green: 0.47058823529411764, blue: 0.5019607843137255, alpha: 0.16)
  }

  public static var tertiarySystemFillCompat: UIColor {
    if #available(iOS 13, *) { return .tertiarySystemFill }
    return UIColor(red: 0.4627450980392157, green: 0.4627450980392157, blue: 0.5019607843137255, alpha: 0.12)
  }

  public static var quaternarySystemFillCompat: UIColor {
    if #available(iOS 13, *) { return .quaternarySystemFill }
    return UIColor(red: 0.4549019607843137, green: 0.4549019607843137, blue: 0.5019607843137255, alpha: 0.08)
  }

  public static var placeholderTextCompat: UIColor {
    if #available(iOS 13, *) { return .placeholderText }
    return UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.3)
  }

  public static var systemBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .systemBackground }
    return UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
  }

  public static var secondarySystemBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .secondarySystemBackground }
    return UIColor(red: 0.9490196078431372, green: 0.9490196078431372, blue: 0.9686274509803922, alpha: 1.0)
  }

  public static var tertiarySystemBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .tertiarySystemBackground }
    return UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
  }

  public static var systemGroupedBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .systemGroupedBackground }
    return UIColor(red: 0.9490196078431372, green: 0.9490196078431372, blue: 0.9686274509803922, alpha: 1.0)
  }

  public static var secondarySystemGroupedBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .secondarySystemGroupedBackground }
    return UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
  }

  public static var tertiarySystemGroupedBackgroundCompat: UIColor {
    if #available(iOS 13, *) { return .tertiarySystemGroupedBackground }
    return UIColor(red: 0.9490196078431372, green: 0.9490196078431372, blue: 0.9686274509803922, alpha: 1.0)
  }

  public static var separatorCompat: UIColor {
    if #available(iOS 13, *) { return .separator }
    return UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.29)
  }

  public static var opaqueSeparatorCompat: UIColor {
    if #available(iOS 13, *) { return .opaqueSeparator }
    return UIColor(red: 0.7764705882352941, green: 0.7764705882352941, blue: 0.7843137254901961, alpha: 1.0)
  }

  public static var linkCompat: UIColor {
    if #available(iOS 13, *) { return .link }
    return UIColor(red: 0.0, green: 0.47843137254901963, blue: 1.0, alpha: 1.0)
  }

  public static var systemIndigoCompat: UIColor {
    if #available(iOS 13, *) { return .systemIndigo }
    return UIColor(red: 0.34509803921568627, green: 0.33725490196078434, blue: 0.8392156862745098, alpha: 1.0)
  }

  public static var systemGray2Compat: UIColor {
    if #available(iOS 13, *) { return .systemGray2 }
    return UIColor(red: 0.6823529411764706, green: 0.6823529411764706, blue: 0.6980392156862745, alpha: 1.0)
  }

  public static var systemGray3Compat: UIColor {
    if #available(iOS 13, *) { return .systemGray3 }
    return UIColor(red: 0.7803921568627451, green: 0.7803921568627451, blue: 0.8, alpha: 1.0)
  }

  public static var systemGray4Compat: UIColor {
    if #available(iOS 13, *) { return .systemGray4 }
    return UIColor(red: 0.8196078431372549, green: 0.8196078431372549, blue: 0.8392156862745098, alpha: 1.0)
  }

  public static var systemGray5Compat: UIColor {
    if #available(iOS 13, *) { return .systemGray5 }
    return UIColor(red: 0.8980392156862745, green: 0.8980392156862745, blue: 0.9176470588235294, alpha: 1.0)
  }

  public static var systemGray6Compat: UIColor {
    if #available(iOS 13, *) { return .systemGray6 }
    return UIColor(red: 0.9490196078431372, green: 0.9490196078431372, blue: 0.9686274509803922, alpha: 1.0)
  }
}
