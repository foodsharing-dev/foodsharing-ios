//
//  FSDateDecoder.swift
//  foodsharing-ios
//
//  Created by Steven on 5/2/20.
//

import Foundation

public extension JSONDecoder.DateDecodingStrategy {
  static var fsJsonDateDecoder: Self {
    .custom(decodeFSDate(from:))
  }
}

private let isoDateFormatter = with(ISO8601DateFormatter()) {
  $0.formatOptions = .withInternetDateTime
}

private func decodeFSDate(from decoder: Decoder) throws -> Date {
  let container = try decoder.singleValueContainer()
  if let doubleValue = try? container.decode(Double.self) {
    return Date(timeIntervalSince1970: doubleValue)
  } else if var stringValue = try? container.decode(String.self).trimmed {
    if let doubleValue = Double(stringValue) {
      return Date(timeIntervalSince1970: doubleValue)
    }
    // remove fraction seconds
    stringValue = stringValue.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
    if let isoDate = isoDateFormatter.date(from: stringValue) {
      return isoDate
    }
  }
  throw DecodingError.typeMismatch(Date.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
}
