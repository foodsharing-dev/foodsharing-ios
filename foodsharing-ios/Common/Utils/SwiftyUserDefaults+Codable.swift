//
//  SwiftyUserDefaults+Codable.swift
//  foodsharing-ios
//
//  Created by Felix Döring on 18.08.22.
//
import SwiftyUserDefaults

extension DefaultsSerializable where Self: Codable {
    public typealias Bridge = DefaultsCodableBridge<Self>
    public typealias ArrayBridge = DefaultsCodableBridge<[Self]>
}

extension DefaultsSerializable where Self: RawRepresentable {
    typealias Bridge = DefaultsRawRepresentableBridge<Self>
    typealias ArrayBridge = DefaultsRawRepresentableArrayBridge<[Self]>
}
