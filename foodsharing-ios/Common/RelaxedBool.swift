//
//  RelaxedBool.swift
//  foodsharing-ios
//
//  Created by Steven on 4/25/20.
//

import Foundation

struct RelaxedBool: Codable {
  let wrappedValue: Bool

  init(_ wrappedValue: Bool) {
    self.wrappedValue = wrappedValue
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()

    if let boolValue = try? container.decode(Bool.self) {
      wrappedValue = boolValue
    } else if let doubleValue = try? container.decode(Double.self),
      let boolValue = Bool(exactly: doubleValue as NSNumber) {
      wrappedValue = boolValue
    } else if let stringValue = try? container.decode(String.self),
      let boolValue = Bool(exactly: (Double(stringValue) ?? 1.1) as NSNumber) {
      wrappedValue = boolValue
    } else {
      throw DecodingError.typeMismatch(Bool.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
    }
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    try container.encode(wrappedValue)
  }
}
