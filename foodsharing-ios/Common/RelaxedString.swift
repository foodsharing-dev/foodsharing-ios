//
//  RelaxedString.swift
//  foodsharing-ios
//
//  Created by Steven on 4/19/20.
//

struct RelaxedString: Codable {
  let wrappedValue: String

  init(_ wrappedValue: String) {
    self.wrappedValue = wrappedValue
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()
    let result: String
    if let stringValue = try? container.decode(String.self) {
      result = stringValue
    } else if let intValue = try? container.decode(Int.self) {
      result = intValue.description
    } else if let doubleValue = try? container.decode(Double.self) {
      result = doubleValue.description
    } else if let boolValue = try? container.decode(Bool.self) {
      result = boolValue.description
    } else {
      throw DecodingError.typeMismatch(String.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
    }
    wrappedValue = result.trimmed
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    try container.encode(wrappedValue)
  }
}
