//
//  constant.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 09.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation
import UIKit

typealias Nibs = R.nib
typealias Colors = R.color
typealias Fonts = R.font
typealias Images = R.image
typealias ReuseIdentifiers = R.reuseIdentifier
typealias Files = R.file
typealias Strings = R.string.localizable

open class MessageKitDateFormatter {

  // MARK: - Properties

  public static let shared = MessageKitDateFormatter()

  private let formatter = DateFormatter()

  // MARK: - Initializer

  private init() {
    formatter.locale = Locale.current
  }

  // MARK: - Methods

  public func string(from date: Date, short: Bool = false) -> String {
    if short {
      configureShortDateFormatter(for: date)
    } else {
      configureDateFormatter(for: date)
    }
    return formatter.string(from: date)
  }

  public func attributedString(from date: Date, with attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
    let dateString = string(from: date)
    return NSAttributedString(string: dateString, attributes: attributes)
  }

  open func configureDateFormatter(for date: Date) {
    let now = Date()
    switch true {
      case Calendar.current.isDateInToday(date) || Calendar.current.isDateInYesterday(date):
        formatter.doesRelativeDateFormatting = true
        formatter.dateStyle = .short
        formatter.timeStyle = .short
      case Calendar.current.isDate(date, equalTo: now, toGranularity: .weekOfYear):
        formatter.dateFormat = "EEEE HH:mm"
      case Calendar.current.isDate(date, equalTo: now, toGranularity: .year):
        formatter.dateFormat = "E dd.MM, HH:mm"
      default:
        switch formatter.locale {
          case Locale(identifier: "en_US"):
            formatter.dateFormat = "MM/dd/yyyy, HH:mm"
          // China, Japan, Korea, Iran would use yyyy/MM/dd but do we need that?
          default:
            formatter.dateFormat = "dd.MM.yyyy, HH:mm"
        }
    }
  }

  open func configureShortDateFormatter(for date: Date) {
    let now = Date()
    switch true {
      case Calendar.current.isDateInToday(date):
        formatter.timeStyle = .short
        formatter.dateStyle = .none
      case Calendar.current.isDateInYesterday(date):
        formatter.timeStyle = .none
        formatter.dateStyle = .medium
        formatter.doesRelativeDateFormatting = true
      case Calendar.current.isDate(date, equalTo: now, toGranularity: .weekOfMonth):
        formatter.dateFormat = "EEEE"
      case Calendar.current.isDate(date, equalTo: now, toGranularity: .year):
        formatter.dateFormat = "d MMM"
      default:
        formatter.timeStyle = .none
        formatter.dateStyle = .short
    }
  }

}

public enum CustomColor {
  static let leaf = UIColor(rgb: 0x518E1D)
  static let incomingGray = UIColor(rgb: 0xE6E6EB)
}

enum Constants {
  //    static let base_url = "http://127.0.0.1:18080";
  static let baseUrl = URL(string: "https://beta.foodsharing.de")!
  static let registerationUrl = URL(string: "https://foodsharing.de/?page=content&sub=joininfo")!
  static let forgotPasswordUrl = URL(string: "https://foodsharing.de/?page=login&sub=passwordReset")!
  //    static let DEFAULT_USER_PICTURE = "http://localhost:18080/img/mini_q_avatar.png"
  static let defaultUserPicutre = baseUrl.appendingPathComponent("/img/130_q_avatar.png")
  static let foodSharingLogoHeight = 39
  static let tokenHeaderName = "X-CSRF-Token"
  static let tokenCookieName = "CSRF_TOKEN"
  static let sessionCookieName = "PHPSESSID"
}

struct BasketDetail {
  var description: String = ""
  var contactTypes: [Int]
  var tel = ""
  var handy = ""
  var weight: Float = 0.0
  var lifetime: Int = 0
  var lat = ""
  var lon = ""
}

extension String {
  /// Converts HTML string to a `NSAttributedString`

  var htmlAttributedString: NSAttributedString? {
    try? NSAttributedString(data: Data(utf8),
                            options: [.documentType: NSAttributedString.DocumentType.html,
                                      .characterEncoding: String.Encoding.utf8.rawValue, ],
                            documentAttributes: nil
    )
  }
}

extension UIColor {
  convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")

    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
  }

  convenience init(rgb: Int) {
    self.init(
      red: (rgb >> 16) & 0xFF,
      green: (rgb >> 8) & 0xFF,
      blue: rgb & 0xFF
    )
  }
}

extension Notification.Name {
  static let gotConvFromSocket = Notification.Name("gotConvFromSocket")
}
