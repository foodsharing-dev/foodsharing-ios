//
//  Keychain.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 15.06.19.
//

import Valet

// if Valet cocoapod is not maintained anymore, we only need to change the functions below
enum Keychain {

  enum Key: String, CaseIterable {
    case tokenCookie
    case sessionIdCookie
    case credentials
  }

  private static let valet = Valet.valet(
    with: Identifier(nonEmpty: "foodsharing")!,
    accessibility: .whenUnlockedThisDeviceOnly
  )

  // Setters

  static func set<T: Encodable>(key: Key, codable value: T?) {
    if let value = value, let data = try? JSONEncoder().encode(value) {
      try? valet.setObject(data, forKey: key.rawValue)
    } else {
      remove(key: key)
    }
  }

  static func set(key: Key, nsCoding value: Any?) {
    if let value = value,
      let data = try? NSKeyedArchiver.archivedData(withRootObject: value,
                                                   requiringSecureCoding: false) {
      try? valet.setObject(data, forKey: key.rawValue)
    } else {
      remove(key: key)
    }
  }

  static func set(key: Key, string value: String?) {
    if let value = value {
      try? valet.setString(value, forKey: key.rawValue)
    } else {
      remove(key: key)
    }
  }

  // Getters

  static func codable<T: Decodable>(key: Key, of type: T.Type = T.self) -> T? {
    guard let data = try? valet.object(forKey: key.rawValue) else { return nil }
    return try? JSONDecoder().decode(T.self, from: data)
  }

  static func nsCoding<T>(key: Key, of type: T.Type = T.self) -> T? {
    guard let data = try? valet.object(forKey: key.rawValue) else { return nil }
    return try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? T
  }

  static func string(key: Key) -> String? {
    return try? valet.string(forKey: key.rawValue)
  }

  // Removers

  static func remove(key: Key) {
    try? valet.removeObject(forKey: key.rawValue)
  }

  static func clear() {
    Key.allCases.forEach { remove(key: $0) }
  }
}
