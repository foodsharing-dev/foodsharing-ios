//
//  Assembler.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import Swinject

extension Assembler {
  static let mainAssembler: Assembler = {
    let container = Container()
    return Assembler([
      APIAssembly(),
      ViewModelAssembly(),
      ViewControllerAssembly(),
    ], container: container)
  }()
}
