//
//  ViewModelAssembly.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import Swinject
import SwinjectAutoregistration

class ViewModelAssembly: Assembly {
  func assemble(container: Container) {
    container.autoregister(SplashVM.self, initializer: SplashVM.init)
    container.autoregister(ConversationsListVM.self, initializer: ConversationsListVM.init)
  }
}
