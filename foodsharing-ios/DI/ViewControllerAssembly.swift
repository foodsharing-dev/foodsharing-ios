//
//  ViewControllerAssembly.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import Swinject
import SwinjectAutoregistration

class ViewControllerAssembly: Assembly {
  func assemble(container: Container) {
    container.autoregister(LoginVC.self, initializer: LoginVC.init)
    container.autoregister(ConversationsListVC.self, initializer: ConversationsListVC.init)
    container.autoregister(MapVC.self, initializer: MapVC.init)
    container.autoregister(BasketsVC.self, initializer: BasketsVC.init)
    container.autoregister(MoreVC.self, initializer: MoreVC.init)
    container.autoregister(ConversationVC.self, initializer: ConversationVC.init)
    container.autoregister(SplashVC.self, initializer: SplashVC.init)
    container.autoregister(MainTabBarController.self, initializer: MainTabBarController.init)
  }
}
