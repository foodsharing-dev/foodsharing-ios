//
//  APIAssembly.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import Swinject
import SwinjectAutoregistration

class APIAssembly: Assembly {
  func assemble(container: Container) {
    container.autoregister(API.self, initializer: DefaultAPI.init).inObjectScope(.container)
  }
}
