//
//  Gravatar.swift
//  foodsharing-ios
//
//  Created by Steven on 4/23/20.
//

import CommonCrypto
import CryptoKit
import Foundation

enum Gravatar {
  enum Format: String {
    case png
    case jpg
  }

  enum FallbackImage: String {
    case fileNotFound = "404"
    case mysteryPerson = "mp"
    case identicon
    case monster = "monsterid"
    case wavatar
    case retro
    case robohash
    case blank
  }

  // swiftlint:disable identifier_name
  enum Rating: String {
    case g
    case pg
    case r
    case x
  }
  // swiftlint:enable identifier_name

  static func url(for email: String,
                  size: Int,
                  rating: Rating = .pg,
                  fallbackImage: FallbackImage? = .identicon,
                  format: Format = .jpg) -> URL {
    let fallbackImage = fallbackImage ?? .fileNotFound
    let size = min(2048, size)
    let email = email.lowercased().trimmed
    guard email.isNotEmpty else { fatalError("Gravatar email cannot be empty") }
    let emailMD5Hash = md5hash(from: email.data(using: .utf8)!)
    var urlString = "https://www.gravatar.com/avatar/\(emailMD5Hash).\(format.rawValue)?"
    urlString += "s=\(size)"
    urlString += "&d=\(fallbackImage.rawValue)"
    urlString += "&r=\(rating.rawValue)"
    return URL(string: urlString)!
  }
}

func md5hash(from data: Data) -> String {
  if #available(iOS 13.0, *) {
    return Insecure.MD5.hash(data: data).map { String(format: "%02hhx", $0) }.joined()
  } else {
    return legacyMD5(input: data).map { String(format: "%02hhx", $0) }.joined()
  }
}

func legacyMD5(input: Data) -> Data {
  let length = Int(CC_MD5_DIGEST_LENGTH)
  var digestData = Data(count: length)
  _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
    input.withUnsafeBytes { messageBytes -> UInt8 in
      if let messageBytesBaseAddress = messageBytes.baseAddress,
        let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
        CC_MD5(messageBytesBaseAddress, CC_LONG(input.count), digestBytesBlindMemory)
      }
      return 0
    }
  }
  return digestData
}
