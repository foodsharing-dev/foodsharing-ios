//
//  DefaultAPI.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import RxSwift

class DefaultAPI: API {

  init() {
    print("init DefaultAPI")
  }

  func listConversations(number: Int, offset: Int) -> Single<ConversationListResponse> {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .fsJsonDateDecoder
    return FSSession.request(APIRouter.listConversations(number: number, offset: offset))
      .rx.responseDecodable(decoder: decoder)
  }

  func currentUser() -> Single<User> {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .fsJsonDateDecoder
    return FSSession.request(APIRouter.getCurrentUser).rx.responseDecodable(decoder: decoder)
  }
}
