//
//  API.swift
//  foodsharing-ios
//
//  Created by Steven on 5/25/20.
//

import RxSwift

protocol API {
  func listConversations(number: Int, offset: Int) -> Single<ConversationListResponse>
  func currentUser() -> Single<User>
}
