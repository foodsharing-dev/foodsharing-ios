//
//  API.swift
//  foodsharing-ios
//
//  Created by Steven on 4/18/20.
//

import Alamofire
import RxSwift
import SwiftyUserDefaults

enum OldAPI {
  static func login(credentials: Credentials, completion:  @escaping (AFDataResponse<User>) -> Void) {
    Keychain.clear()
    PersistentCookieStore.sharedStore.clear()
    FSSession.request(APIRouter.login(credentials: credentials))
      .responseDecodable(of: User.self, completionHandler: completion)
  }

  static func conversationDetail(id: Int,
                                 number: Int = 20,
                                 offset: Int = 0,
                                 completion:  @escaping (Result<ConversationDetailResponse, AFError>) -> Void) {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .fsJsonDateDecoder
    FSSession.request(APIRouter.getConversation(id: id, number: number)).responseDecodable(decoder: decoder) {
        completion($0.result)
    }
  }
}
