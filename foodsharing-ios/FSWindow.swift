//
//  FSWindow.swift
//  foodsharing-ios
//
//  Created by Steven on 6/6/20.
//

import UIKit

class FSWindow: UIWindow {

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }

  @available(iOS 13.0, *)
  override init(windowScene: UIWindowScene) {
    super.init(windowScene: windowScene)
    commonInit()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    commonInit()
  }

  func commonInit() {
    #if DEBUG
    installFlexRecognizer()
    #endif
  }

  func setAnimatedRootVC(viewController: UIViewController?) {
    self.rootViewController = viewController
    if rootViewController != nil {
      UIView.transition(
        with: self,
        duration: 0.25,
        options: .transitionCrossDissolve,
        animations: { }
      )
    }
  }
}

#if DEBUG
import FLEX

extension FSWindow {
  func installFlexRecognizer() {
    let recognizer = UISwipeGestureRecognizer(target: self, action: #selector(launchFlex))
    recognizer.direction = .down
    recognizer.numberOfTouchesRequired = 3
    recognizer.delegate = self
    addGestureRecognizer(recognizer)
  }

  @objc
  func launchFlex() {
    FLEXManager.shared.isNetworkDebuggingEnabled = true
    if #available(iOS 13.0, *), let windowScene = windowScene {
      FLEXManager.shared.showExplorer(from: windowScene)
    } else {
      FLEXManager.shared.showExplorer()
    }
  }
}

extension FSWindow: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                         shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
}
#endif
