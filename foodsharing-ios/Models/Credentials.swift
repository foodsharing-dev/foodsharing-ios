//
//  Credentials.swift
//  foodsharing-ios
//
//  Created by Steven on 6/5/20.
//

import Foundation

struct Credentials {
  let email: String
  let password: String
}

extension Credentials: Codable {
}
