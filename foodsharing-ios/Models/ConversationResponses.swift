//
//  ConversationResponses.swift
//  foodsharing-ios
//
//  Created by Steven on 5/16/20.
//

// swiftlint:disable file_types_order

import BetterCodable
import Foundation

struct ConversationListResponse: Codable {
  private let conversations: [ConversationResponse]
  private let profiles: [User]

  func toConversations() -> [Conversation] {
    return conversations.map { $0.toConversation(from: profiles) }
  }
}

struct ConversationDetailResponse: Codable {
  private let conversation: ConversationResponse
  private let profiles: [User]

  func toConversation() -> Conversation {
    return conversation.toConversation(from: profiles)
  }
}

struct SocketResponse {
  @LosslessValue private(set) var cid: Int
  private let message: MessageResponse

  func toMessage(from users: [User]) -> Message {
    return message.toMessage(from: users)
  }
}

extension SocketResponse: Codable {
}

private struct ConversationResponse {
  @LosslessValue private(set) var id: Int
  @LosslessValue private(set) var hasUnreadMessages: Bool
  let title: String?
  let members: [Int]
  let lastMessage: MessageResponse?
  @DefaultEmptyArray private(set) var messages: [MessageResponse]

  fileprivate func toConversation(from users: [User]) -> Conversation {
    let messages = self.messages.map { $0.toMessage(from: users) }.sorted(on: \.sentAt, by: <)
    return Conversation(
      id: id,
      title: title,
      hasUnreadMessages: hasUnreadMessages,
      members: members.map { users.getUser(by: $0) },
      lastMessage: lastMessage?.toMessage(from: users),
      messages: messages
    )
  }
}

extension ConversationResponse: Codable {
}

private struct MessageResponse {
  @LosslessValue private(set) var id: Int
  @LosslessValue private(set) var authorId: Int
  let body: String
  let sentAt: Date

  func toMessage(from users: [User]) -> Message {
    return Message(
      id: id,
      body: body,
      sentAt: sentAt,
      author: users.getUser(by: authorId)
    )
  }
}

extension MessageResponse: Codable {
}

private extension Array where Element == User {
  func getUser(by id: Int) -> User {
    return first { $0.id == id }!
  }
}
