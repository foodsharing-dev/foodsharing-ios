//
//  Message.swift
//  foodsharing-ios
//
//  Created by Steven on 5/16/20.
//

import Foundation
import MessageKit

struct Message {
  let id: Int
  let body: String
  let sentAt: Date
  let author: User
}

extension Message: MessageType {
  var sender: SenderType { author }
  var messageId: String { String(id) }
  var sentDate: Date { sentAt }
  var kind: MessageKind { .text(body) }
}
