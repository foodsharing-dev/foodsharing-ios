//
//  Conversation.swift
//  foodsharing-ios
//
//  Created by Steven on 5/16/20.
//

struct Conversation {
  let id: Int
  let title: String?
  let hasUnreadMessages: Bool
  let members: [User]
  let lastMessage: Message?
  var messages: [Message]
}
