//
//  UserProfile.swift
//  foodsharing-ios
//
//  Created by Steven on 4/18/20.
//

import BetterCodable
import MessageKit
import SwiftyUserDefaults

struct User {
  @LosslessValue private(set) var id: Int
  let avatar: String?
  let name: String?
  let sleepStatus: Int?
}

extension User: Codable {
}

extension User: DefaultsSerializable {
}

extension User: SenderType {
  var senderId: String { String(id) }
  var displayName: String { name?.trimmed ?? "" }
}
